package no.accelerate.classes;

import no.accelerate.character.Character;
import no.accelerate.character.PrimaryAttributes;
import no.accelerate.equipment.Armour;
import no.accelerate.equipment.Item;
import no.accelerate.equipment.Weapon;
import no.accelerate.exceptions.InvalidArmourException;
import no.accelerate.exceptions.InvalidLevelException;
import no.accelerate.exceptions.InvalidWeaponException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WarriorTest {
    @Test
    void newMage_withoutEquipment_ShouldHaveBaseDps() {

        String  nameOfCharacter = "Axl";

        Character warrior = new Warrior(nameOfCharacter);

        PrimaryAttributes attributes = new PrimaryAttributes(5, 2, 1);

        double expected = 1 * (1 + (attributes.getStrength()/100));
        double actual = warrior.getDps();

        assertEquals(expected, actual);
    }

    @Test
    void checkDps_withWeapon_shouldBeHigher() throws InvalidWeaponException, InvalidLevelException {

        String  nameOfCharacter = "Axl";


        String nameOfWeapon = "Common Sword";
        PrimaryAttributes weaponAttributes = new PrimaryAttributes(1, 1, 1);
        int requiredLevel = 1;
        Weapon.WeaponType weaponType =  Weapon.WeaponType.Swords;
        int damageOfWeapon = 10;
        double attackSpeedOfWeapon = 0.2;

        Weapon commonSword = new Weapon(
                nameOfWeapon,
                weaponAttributes,
                requiredLevel,
                weaponType,
                damageOfWeapon,
                attackSpeedOfWeapon);

        Character warrior = new Warrior(nameOfCharacter);

        PrimaryAttributes attributes = new PrimaryAttributes(5, 2, 1);
        warrior.equipWeapon(commonSword);

        double expected = commonSword.dps * (1 + ( attributes.getStrength()/100));
        double actual = warrior.getDps();

        assertEquals(expected, actual);
    }

    @Test
    void checkDps_withWeaponAndArmour_shouldBeHigher() throws InvalidWeaponException, InvalidLevelException, InvalidArmourException {

        String  nameOfCharacter = "Axl";

        String nameOfWeapon = "Common Sword";
        PrimaryAttributes weaponAttributes = new PrimaryAttributes(1, 1, 1);
        int requiredLevelWeapon = 1;
        Weapon.WeaponType weaponType =  Weapon.WeaponType.Swords;
        int damageOfWeapon = 10;
        double attackSpeedOfWeapon = 0.2;

        String nameOfArmour = "Scuffed Plate";
        PrimaryAttributes armourAttributes = new PrimaryAttributes(3, 1, 1);
        int requiredLevelArmour = 1;
        Armour.ArmourType armourType = Armour.ArmourType.Plate;
        Item.ItemSlot armourSlot = Item.ItemSlot.Body;


        Weapon commonSword = new Weapon(
                nameOfWeapon,
                weaponAttributes,
                requiredLevelWeapon,
                weaponType,
                damageOfWeapon,
                attackSpeedOfWeapon);


        Armour scuffedPlate = new Armour(
                nameOfArmour,
                requiredLevelArmour,
                armourAttributes,
                armourType,
                armourSlot);

        Character warrior = new Warrior(nameOfCharacter);
        warrior.equipWeapon(commonSword);
        warrior.equipArmour(scuffedPlate);

        PrimaryAttributes attributes = new PrimaryAttributes(5, 2, 1);
        attributes.raiseAttributes(3,1,1);

        double expected = (commonSword.dps * (1 + ( attributes.getStrength()/100)));
        double actual = warrior.getDps();

        assertEquals(expected, actual);
    }
}