package no.accelerate.classes;

import no.accelerate.character.Character;
import no.accelerate.character.PrimaryAttributes;
import no.accelerate.equipment.Armour;
import no.accelerate.equipment.Item;
import no.accelerate.equipment.Weapon;
import no.accelerate.exceptions.InvalidArmourException;
import no.accelerate.exceptions.InvalidLevelException;
import no.accelerate.exceptions.InvalidWeaponException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RogueTest {
    @Test
    void newMage_withoutEquipment_ShouldHaveBaseDps() {

        String  nameOfCharacter = "Axl";

        Character rogue = new Rogue(nameOfCharacter);

        PrimaryAttributes attributes = new PrimaryAttributes(2, 6, 1);

        double expected = 1 * (1 + (attributes.getDexterity()/100));
        double actual = rogue.getDps();

        assertEquals(expected, actual);
    }

    @Test
    void checkDps_withWeapon_shouldBeHigher() throws InvalidWeaponException, InvalidLevelException {

        String  nameOfCharacter = "Axl";

        String nameOfWeapon = "Common Dagger";
        PrimaryAttributes weaponAttributes = new PrimaryAttributes(1, 1, 1);
        int requiredLevel = 1;
        Weapon.WeaponType weaponType =  Weapon.WeaponType.Daggers;
        int damageOfWeapon = 10;
        double attackSpeedOfWeapon = 0.5;

        Weapon commonDagger = new Weapon(
                nameOfWeapon,
                weaponAttributes,
                requiredLevel,
                weaponType,
                damageOfWeapon,
                attackSpeedOfWeapon);

        Character rogue = new Rogue(nameOfCharacter);

        PrimaryAttributes attributes = new PrimaryAttributes(2, 6, 1);
        rogue.equipWeapon(commonDagger);

        double expected = commonDagger.dps * (1 + (attributes.getDexterity()/100));
        double actual = rogue.getDps();

        assertEquals(expected, actual);
    }

    @Test
    void checkDps_withWeaponAndArmour_shouldBeHigher() throws InvalidWeaponException, InvalidLevelException, InvalidArmourException {

        String  nameOfCharacter = "Axl";

        String nameOfWeapon = "Common Dagger";
        PrimaryAttributes weaponAttributes = new PrimaryAttributes(1, 1, 1);
        int requiredLevelWeapon = 1;
        Weapon.WeaponType weaponType =  Weapon.WeaponType.Daggers;
        int damageOfWeapon = 10;
        double attackSpeedOfWeapon = 0.1;

        String nameOfArmour = "Common Tunic";
        PrimaryAttributes armourAttributes = new PrimaryAttributes(1, 3, 1);
        int requiredLevelArmour = 1;
        Armour.ArmourType armourType = Armour.ArmourType.Leather;
        Item.ItemSlot armourSlot = Item.ItemSlot.Body;


        Weapon commonDagger = new Weapon(
                nameOfWeapon,
                weaponAttributes,
                requiredLevelWeapon,
                weaponType,
                damageOfWeapon,
                attackSpeedOfWeapon);


        Armour commonTunic = new Armour(
                nameOfArmour,
                requiredLevelArmour,
                armourAttributes,
                armourType,
                armourSlot);

        Character rogue = new Rogue(nameOfCharacter);
        rogue.equipWeapon(commonDagger);
        rogue.equipArmour(commonTunic);

        PrimaryAttributes attributes = new PrimaryAttributes(2, 6, 1);
        attributes.raiseAttributes(1,3,1);

        double expected = (commonDagger.dps * (1 + ( attributes.getDexterity()/100)));
        double actual = rogue.getDps();

        assertEquals(expected, actual);
    }
}