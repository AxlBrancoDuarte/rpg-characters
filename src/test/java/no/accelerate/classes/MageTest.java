package no.accelerate.classes;

import no.accelerate.character.Character;
import no.accelerate.character.PrimaryAttributes;
import no.accelerate.equipment.Armour;
import no.accelerate.equipment.Item;
import no.accelerate.equipment.Weapon;
import no.accelerate.exceptions.InvalidArmourException;
import no.accelerate.exceptions.InvalidLevelException;
import no.accelerate.exceptions.InvalidWeaponException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MageTest {

    @Test
    void newMage_withoutEquipment_ShouldHaveBaseDps() {

        String  nameOfCharacter = "Axl";

        Character mage = new Mage(nameOfCharacter);

        PrimaryAttributes attributes = new PrimaryAttributes(1, 1, 8);

        double expected = 1 * (1 + (attributes.getIntelligence()/100));
        double actual = mage.getDps();

        assertEquals(expected, actual);
        }

    @Test
    void checkDps_withWeapon_shouldBeHigher() throws InvalidWeaponException, InvalidLevelException {

        String  nameOfCharacter = "Axl";
        Character mage = new Mage(nameOfCharacter);

        PrimaryAttributes attributes = new PrimaryAttributes(1, 1, 8);

        String nameOfWeapon = "Common Staff";
        PrimaryAttributes weaponAttributes = new PrimaryAttributes(1, 1, 1);
        int requiredLevel = 1;
        Weapon.WeaponType weaponType =  Weapon.WeaponType.Staffs;
        int damageOfWeapon = 10;
        double attackSpeedOfWeapon = 0.5;

        Weapon commonStaff = new Weapon(
                nameOfWeapon,
                weaponAttributes,
                requiredLevel,
                weaponType,
                damageOfWeapon,
                attackSpeedOfWeapon);

        mage.equipWeapon(commonStaff);

        double expected = commonStaff.dps * (1 + ( attributes.getIntelligence()/100));
        double actual = mage.getDps();

        assertEquals(expected, actual);
    }

    @Test
    void checkDps_withWeaponAndArmour_shouldBeHigher() throws InvalidWeaponException, InvalidLevelException, InvalidArmourException {

        String  nameOfCharacter = "Axl";

        String nameOfWeapon = "Common Staff";
        PrimaryAttributes weaponAttributes = new PrimaryAttributes(1, 1, 1);
        int requiredLevelWeapon = 1;
        Weapon.WeaponType weaponType =  Weapon.WeaponType.Staffs;
        int damageOfWeapon = 10;
        double attackSpeedOfWeapon = 0.5;

        String nameOfArmour = "Common Robe";
        PrimaryAttributes armourAttributes = new PrimaryAttributes(1, 1, 3);
        int requiredLevelArmour = 1;
        Armour.ArmourType armourType = Armour.ArmourType.Cloth;
        Item.ItemSlot armourSlot = Item.ItemSlot.Body;


        Weapon commonStaff = new Weapon(
                nameOfWeapon,
                weaponAttributes,
                requiredLevelWeapon,
                weaponType,
                damageOfWeapon,
                attackSpeedOfWeapon);


        Armour commonRobe = new Armour(
                nameOfArmour,
                requiredLevelArmour,
                armourAttributes,
                armourType,
                armourSlot);

        Character mage = new Mage(nameOfCharacter);
        mage.equipWeapon(commonStaff);
        mage.equipArmour(commonRobe);

        PrimaryAttributes attributes = new PrimaryAttributes(1, 1, 8);
        attributes.raiseAttributes(1,1,3);

        double expected = commonStaff.dps * (1 + ( attributes.getIntelligence()/100));
        double actual = mage.getDps();

        assertEquals(expected, actual);
    }


    }
