package no.accelerate.classes;

import no.accelerate.character.Character;
import no.accelerate.character.PrimaryAttributes;
import no.accelerate.equipment.Armour;
import no.accelerate.equipment.Item;
import no.accelerate.equipment.Weapon;
import no.accelerate.exceptions.InvalidArmourException;
import no.accelerate.exceptions.InvalidLevelException;
import no.accelerate.exceptions.InvalidWeaponException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PlayableCharacterTest {

    @Test
    public void createCharacter_shouldCreatCharacterAtLevel1() {

        String  nameOfCharacter = "Axl";
        Character mage = new Mage(nameOfCharacter);
        int expected = 1;

        int actual = mage.getLevel();

        assertEquals(expected, actual);
    }

    @Test
    public void levelUp_shouldIncreaseCharacterLevelBy1() {

        String  nameOfCharacter = "Axl";
        Character mage = new Mage(nameOfCharacter);
        int expected = 2;

        mage.levelUp();
        int actual = mage.getLevel();

        assertEquals(expected,actual);

    }

    @Test
    public void equipWeapon_validWeapon_shouldReturnTrueIfEquipped() throws InvalidWeaponException, InvalidLevelException {

        String  nameOfCharacter = "Axl";
        Character mage = new Mage(nameOfCharacter);

        String nameOfWeapon = "Common Staff";
        PrimaryAttributes weaponAttributes = new PrimaryAttributes(1, 1, 1);
        int requiredLevel = 1;
        Weapon.WeaponType weaponType =  Weapon.WeaponType.Staffs;
        int damageOfWeapon = 10;
        double attackSpeedOfWeapon = 0.5;

        Weapon commonStaff = new Weapon(
                nameOfWeapon,
                weaponAttributes,
                requiredLevel,
                weaponType,
                damageOfWeapon,
                attackSpeedOfWeapon);

        boolean expected = true;
        boolean actual = mage.equipWeapon(commonStaff);


        assertEquals(expected,actual);
    }

    @Test
    public void equipWeapon_invalidWeaponLevel_shouldThrowLevelException() {

        String  nameOfCharacter = "Axl";
        Character mage = new Mage(nameOfCharacter);

        String nameOfWeapon = "Common Staff";
        PrimaryAttributes weaponAttributes = new PrimaryAttributes(1, 1, 1);
        int requiredLevel = 2;
        Weapon.WeaponType weaponType =  Weapon.WeaponType.Staffs;
        int damageOfWeapon = 10;
        double attackSpeedOfWeapon = 0.5;

        Weapon staff = new Weapon(
                nameOfWeapon,
                weaponAttributes,
                requiredLevel,
                weaponType,
                damageOfWeapon,
                attackSpeedOfWeapon);

        Exception exception = assertThrows(InvalidLevelException.class, () -> mage.equipWeapon(staff));

        String expected = "A level \"1\" player cannot equip gear with level requirement of level \"2\"";
        String actual = exception.getMessage();

        assertEquals(expected,actual);
    }

    @Test
    public void equipWeapon_invalidWeaponType_shouldThrowWeaponException() {

        String  nameOfCharacter = "Axl";
        Character mage = new Mage(nameOfCharacter);

        String nameOfWeapon = "Blunt Axe";
        PrimaryAttributes weaponAttributes = new PrimaryAttributes(1, 1, 1);
        int requiredLevel = 1;
        Weapon.WeaponType weaponType =  Weapon.WeaponType.Axes;
        int damageOfWeapon = 15;
        double attackSpeedOfWeapon = 0.2;

        Weapon bluntAxe = new Weapon(
                nameOfWeapon,
                weaponAttributes,
                requiredLevel,
                weaponType,
                damageOfWeapon,
                attackSpeedOfWeapon);

        Exception exception = assertThrows(InvalidWeaponException.class, () -> mage.equipWeapon(bluntAxe));

        String expected = "\"Mage\" cannot equip weapon type \"Axes\"";
        String actual = exception.getMessage();

        assertEquals(expected,actual);
    }

    @Test
    public void equipArmour_validArmour_shouldReturnTrueIfEquipped() throws InvalidLevelException, InvalidArmourException {

        String  nameOfCharacter = "Axl";
        Character mage = new Mage(nameOfCharacter);

        String nameOfArmour = "Common Robe";
        PrimaryAttributes armourAttributes = new PrimaryAttributes(1, 1, 3);
        int requiredLevel = 1;
        Armour.ArmourType armourType = Armour.ArmourType.Cloth;
        Item.ItemSlot armourSlot = Item.ItemSlot.Body;



        Armour commonRobe = new Armour(
                nameOfArmour,
                requiredLevel,
                armourAttributes,
                armourType,
                armourSlot);

        boolean expected = true;
        boolean actual = mage.equipArmour(commonRobe);

        assertEquals(expected,actual);
    }

    @Test
    public void equipArmour_invalidArmourLevel_shouldReturnLevelException(){

        String  nameOfCharacter = "Axl";
        Character mage = new Mage(nameOfCharacter);

        String nameOfArmour = "Common Robe";
        PrimaryAttributes armourAttributes = new PrimaryAttributes(1, 1, 3);
        int requiredLevel = 2;
        Armour.ArmourType armourType = Armour.ArmourType.Cloth;
        Item.ItemSlot armourSlot = Item.ItemSlot.Body;



        Armour commonRobe = new Armour(
                nameOfArmour,
                requiredLevel,
                armourAttributes,
                armourType,
                armourSlot);

        Exception exception = assertThrows(InvalidLevelException.class, () -> mage.equipArmour(commonRobe));

        String expected = "A level \"1\" player cannot equip gear with level requirement of level \"2\"";
        String actual = exception.getMessage();

        assertEquals(expected,actual);
    }

    @Test
    public void equipArmour_invalidArmourType_shouldReturnLevelException() {

        String  nameOfCharacter = "Axl";
        Character mage = new Mage(nameOfCharacter);

        String nameOfArmour = "Common Tunic";
        PrimaryAttributes armourAttributes = new PrimaryAttributes(1, 3, 1);
        int requiredLevel = 1;
        Armour.ArmourType armourType = Armour.ArmourType.Leather;
        Item.ItemSlot armourSlot = Item.ItemSlot.Body;



        Armour commonTunic = new Armour(
                nameOfArmour,
                requiredLevel,
                armourAttributes,
                armourType,
                armourSlot);

        Exception exception = assertThrows(InvalidArmourException.class, () -> mage.equipArmour(commonTunic));

        String expected = "\"Mage\" cannot equip armour type \"Leather\"";
        String actual = exception.getMessage();

        assertEquals(expected,actual);
    }


}
//    assertEquals("Name:  Axl, Strength: 1, Dexterity: 1, Intelligence: 8", newMage.displayStats());
