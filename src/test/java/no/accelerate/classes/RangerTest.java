package no.accelerate.classes;

import no.accelerate.character.Character;
import no.accelerate.character.PrimaryAttributes;
import no.accelerate.equipment.Armour;
import no.accelerate.equipment.Item;
import no.accelerate.equipment.Weapon;
import no.accelerate.exceptions.InvalidArmourException;
import no.accelerate.exceptions.InvalidLevelException;
import no.accelerate.exceptions.InvalidWeaponException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RangerTest {
    @Test
    void newMage_withoutEquipment_ShouldHaveBaseDps() {

        String  nameOfCharacter = "Axl";

        Character ranger = new Ranger(nameOfCharacter);

        PrimaryAttributes attributes = new PrimaryAttributes(1, 7, 1);

        double expected = 1 * (1 + (attributes.getDexterity())/100);
        double actual = ranger.getDps();

        assertEquals(expected, actual);
    }

    @Test
    void checkDps_withWeapon_shouldBeHigher() throws InvalidWeaponException, InvalidLevelException {

        String  nameOfCharacter = "Axl";

        String nameOfWeapon = "Common Bow";
        PrimaryAttributes weaponAttributes = new PrimaryAttributes(1, 1, 1);
        int requiredLevel = 1;
        Weapon.WeaponType weaponType =  Weapon.WeaponType.Bows;
        int damageOfWeapon = 10;
        double attackSpeedOfWeapon = 0.5;

        Weapon commonBow = new Weapon(
                nameOfWeapon,
                weaponAttributes,
                requiredLevel,
                weaponType,
                damageOfWeapon,
                attackSpeedOfWeapon);

        Character ranger = new Ranger(nameOfCharacter);

        PrimaryAttributes attributes = new PrimaryAttributes(1, 7, 1);
        ranger.equipWeapon(commonBow);

        double expected = commonBow.dps * (1 + (attributes.getDexterity()/100));
        double actual = ranger.getDps();

        assertEquals(expected, actual);
    }

    @Test
    void checkDps_withWeaponAndArmour_shouldBeHigher() throws InvalidWeaponException, InvalidLevelException, InvalidArmourException {

        String  nameOfCharacter = "Axl";

        String nameOfWeapon = "Common Bow";
        PrimaryAttributes weaponAttributes = new PrimaryAttributes(1, 1, 1);
        int requiredLevelWeapon = 1;
        Weapon.WeaponType weaponType =  Weapon.WeaponType.Bows;
        int damageOfWeapon = 10;
        double attackSpeedOfWeapon = 0.5;

        String nameOfArmour = "Rugged Mail";
        PrimaryAttributes armourAttributes = new PrimaryAttributes(1, 1, 3);
        int requiredLevelArmour = 1;
        Armour.ArmourType armourType = Armour.ArmourType.Mail;
        Item.ItemSlot armourSlot = Item.ItemSlot.Body;


        Weapon commonBow = new Weapon(
                nameOfWeapon,
                weaponAttributes,
                requiredLevelWeapon,
                weaponType,
                damageOfWeapon,
                attackSpeedOfWeapon);


        Armour ruggedMail = new Armour(
                nameOfArmour,
                requiredLevelArmour,
                armourAttributes,
                armourType,
                armourSlot);

        Character ranger = new Ranger(nameOfCharacter);
        ranger.equipWeapon(commonBow);
        ranger.equipArmour(ruggedMail);

        PrimaryAttributes attributes = new PrimaryAttributes(1, 1, 8);
        attributes.raiseAttributes(1,1,3);

        double expected = commonBow.dps * (1 + ( attributes.getIntelligence()/100));
        double actual = ranger.getDps();

        assertEquals(expected, actual);
    }
}