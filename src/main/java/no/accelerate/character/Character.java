package no.accelerate.character;

import no.accelerate.equipment.Armour;
import no.accelerate.equipment.Item;
import no.accelerate.equipment.Weapon;
import no.accelerate.exceptions.InvalidArmourException;
import no.accelerate.exceptions.InvalidLevelException;
import no.accelerate.exceptions.InvalidWeaponException;

import java.util.Arrays;
import java.util.HashMap;

public abstract class Character {

    //Variables / Attributes all characters have
    public enum CharacterClass
    {
        Mage,
        Ranger,
        Rogue,
        Warrior
    }
    private final String name;
    public String getName() {
        return name;
    }
    private int level;
    public int getLevel() {
        return level;
    }
    public double dps;
    public double getDps() {
        return dps;
    }
    private final CharacterClass profession;
    public CharacterClass getProfession() {
        return profession;
    }

    public final PrimaryAttributes baseAttributes;
    public PrimaryAttributes totalAttributes;

    public HashMap<Item.ItemSlot, Item> equipment;
    public Armour.ArmourType[] AllowedArmour;
    public Weapon.WeaponType[] AllowedWeapons;

    //Constructor for character
    public Character(String name,CharacterClass profession,  int strength, int dexterity, int intelligence) {
        this.name = name;
        this.profession = profession;
        this.level = 1;

        baseAttributes = new PrimaryAttributes(strength,dexterity,intelligence);
        totalAttributes = new PrimaryAttributes(strength,dexterity,intelligence);

        this.equipment = new HashMap<>();

        AllowedArmour = new Armour.ArmourType[0];
        AllowedWeapons = new Weapon.WeaponType[0];

        calculateDps();
    }

    //Methods all characters have
    public abstract void levelUp();
    public void gainLevel() {
        this.level++;
    }
    public abstract void calculateDps();
    public void displayStats(){
        System.out.println("Name: " + this.getName());
        System.out.println("Class: " + this.getProfession());
        System.out.println("Level:" + this.getLevel());
        System.out.println("Strength:" + this.totalAttributes.getStrength());
        System.out.println("Dexterity:" + this.totalAttributes.getDexterity());
        System.out.println("Intelligence:" + this.totalAttributes.getIntelligence());
        System.out.println("DPS:" + this.getDps());
    }


    public boolean equipArmour(Armour armour) throws InvalidArmourException, InvalidLevelException {
        boolean levelTooLow = this.level < armour.getRequiredLevel();
        boolean invalidArmourType = Arrays.stream(AllowedArmour).noneMatch(type -> type == armour.getType());

        if (invalidArmourType){
            throw new InvalidArmourException(profession.toString(), armour.getType().toString());
        }else if (levelTooLow) {
            throw new InvalidLevelException(this.getLevel(), armour.getRequiredLevel());
        }

         equipment.put(armour.getSlot(), armour);
         CalculateAttributes();

        return true;
    }

    public boolean equipWeapon(Weapon weapon) throws InvalidWeaponException,InvalidLevelException {
        boolean levelTooLow = this.level < weapon.getRequiredLevel();
        boolean invalidWeaponType = Arrays.stream(AllowedWeapons).noneMatch(type -> type == weapon.getType());

        if (invalidWeaponType) {
            throw new InvalidWeaponException(profession.toString(), weapon.getType().toString());
        }else if (levelTooLow) {
            throw new InvalidLevelException(this.getLevel(), weapon.getRequiredLevel());
        }
        equipment.put(weapon.getSlot(), weapon);
        CalculateAttributes();

        return true;
    }

    private void CalculateAttributes()
    {
        this.totalAttributes = new PrimaryAttributes(0, 0, 0);

        this.totalAttributes.raiseAttributes(baseAttributes.getStrength(), baseAttributes.getDexterity(), baseAttributes.getIntelligence());

        this.equipment.forEach((slot, item) -> {
            if (slot == Item.ItemSlot.Weapon) return;
            if (item == null) return;
            PrimaryAttributes itemAttributes = item.getAttributes();
            this.totalAttributes.raiseAttributes(itemAttributes.getStrength(), itemAttributes.getDexterity(), itemAttributes.getIntelligence());
        });
        calculateDps();
    }

}



