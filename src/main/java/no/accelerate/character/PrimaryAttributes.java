package no.accelerate.character;

public class PrimaryAttributes {
    public PrimaryAttributes(int strength, int dexterity, int intelligence) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    private int strength;
    private int dexterity;
    private int intelligence;

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public void raiseAttributes(int strength, int dexterity, int intelligence) {
        this.setStrength(this.getStrength() + strength);
        this.setDexterity(this.getDexterity() + dexterity);
        this.setIntelligence(this.getIntelligence() + intelligence);
    }

    public String characterStats(){
        return ", Strength: "+this.getStrength() +", Dexterity: "+ this.getDexterity() +", Intelligence: "+ this.getIntelligence();
    }
}
