package no.accelerate.classes;
import no.accelerate.character.Character;
import no.accelerate.equipment.Armour;
import no.accelerate.equipment.Item;
import no.accelerate.equipment.Weapon;

public class Ranger extends Character{
    private static final int STARTING_STRENGTH = 1;
    private static final int STARTING_DEXTERITY = 7;
    private static final int STARTING_INTELLIGENCE = 1;
    private static final int LVL_STRENGTH = 1;
    private static final int LVL_DEXTERITY = 5;
    private static final int LVL_INTELLIGENCE = 1;

    public Ranger(String name) {
        super(name, CharacterClass.Mage, STARTING_STRENGTH, STARTING_DEXTERITY, STARTING_INTELLIGENCE);

        AllowedArmour = new Armour.ArmourType[] { Armour.ArmourType.Leather, Armour.ArmourType.Mail };
        AllowedWeapons = new Weapon.WeaponType[] { Weapon.WeaponType.Bows };

    }

    @Override
    public void levelUp() {
        this.gainLevel();
        this.baseAttributes.raiseAttributes(LVL_STRENGTH, LVL_DEXTERITY, LVL_INTELLIGENCE);
    }

    @Override
    public void calculateDps()
    {
        double offset = 1;
        double weaponDps = 1;

        if (equipment.containsKey(Item.ItemSlot.Weapon))
        {
            Weapon weapon = (Weapon)equipment.get(Item.ItemSlot.Weapon);

            weaponDps = weapon.getDps();
        }

        this.dps = (weaponDps * (offset + (totalAttributes.getDexterity() / 100)));
    }
}
