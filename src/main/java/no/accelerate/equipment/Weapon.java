package no.accelerate.equipment;

import no.accelerate.character.PrimaryAttributes;

public class Weapon extends Item{
    public enum WeaponType
    {
        Axes,
        Bows,
        Daggers,
        Hammers,
        Staffs,
        Swords,
        Wands
    }
    public WeaponType type;
    public WeaponType getType() {
        return type;
    }
    public double damage;
    public double attackSpeed;
    public double dps;
    public double getDps() {
        return dps;
    }

    public Weapon(String name, PrimaryAttributes attributes, int requiredLevel, WeaponType type, double damage, double attackSpeed) {
        super(name, requiredLevel, attributes, ItemSlot.Weapon);

        this.type = type;
        this.damage = damage;
        this.attackSpeed = attackSpeed;
        this.dps = damage * attackSpeed;
    }
}
