package no.accelerate.equipment;

import no.accelerate.character.PrimaryAttributes;

public abstract class Item {
    public enum ItemSlot
    {
        Head,
        Body,
        Legs,
        Weapon
    }
    private final String name;
    public PrimaryAttributes attributes;
    private final int requiredLevel;
    private final ItemSlot slot;

    public String getName() {
        return name;
    }

    public PrimaryAttributes getAttributes() {
        return attributes;
    }

    public int getRequiredLevel() {
        return requiredLevel;
    }

    public ItemSlot getSlot() {
        return slot;
    }

    public Item(String name, int requiredLevel, PrimaryAttributes attributes, ItemSlot slot) {
        this.name = name;
        this.attributes = attributes;
        this.requiredLevel = requiredLevel;
        this.slot = slot;

    }
}
