package no.accelerate.equipment;

import no.accelerate.character.PrimaryAttributes;

public class Armour extends Item{
    public enum ArmourType
    {
        Cloth,
        Leather,
        Mail,
        Plate
    }

    private PrimaryAttributes attributes;
    private ArmourType type;

    @Override
    public PrimaryAttributes getAttributes() {
        return attributes;
    }

    public void setAttributes(PrimaryAttributes attributes) {
        this.attributes = attributes;
    }

    public ArmourType getType() {
        return type;
    }

    public void setType(ArmourType type) {
        this.type = type;
    }

    public Armour(String name, int requiredLevel, PrimaryAttributes attributes, ArmourType type, ItemSlot slot) {
        super(name, requiredLevel, attributes, slot);
        this.attributes = attributes;
        this.type = type;
    }


}
