package no.accelerate;

import no.accelerate.character.Character;
import no.accelerate.classes.Mage;
import no.accelerate.equipment.Weapon;

public class Main {
    public static void main(String[] args) {
        Character mage = new Mage("Axl");
        mage.displayStats();
    }
}
