package no.accelerate.exceptions;

public class InvalidLevelException extends Exception{
    public InvalidLevelException(int playerLevel, int gearLevelRequirment) {
        super("A level \"" + playerLevel+ "\" player cannot equip gear with level requirement of level \"" + gearLevelRequirment + "\"");
    }
}
