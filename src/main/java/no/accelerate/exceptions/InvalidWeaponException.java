package no.accelerate.exceptions;

    public class InvalidWeaponException extends Exception {
        public InvalidWeaponException(String profession, String weaponType) {
            super("\""+ profession+ "\" cannot equip weapon type \"" + weaponType + "\"");
        }
    }
