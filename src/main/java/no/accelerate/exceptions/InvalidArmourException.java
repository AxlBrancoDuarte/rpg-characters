package no.accelerate.exceptions;

public class InvalidArmourException extends Exception {
    public InvalidArmourException(String profession, String armourType) {
        super("\""+ profession+ "\" cannot equip armour type \"" + armourType + "\"");
    }
}
