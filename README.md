# Automated testing with Gradle and JUnit

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![pipeline status](https://gitlab.com/NicholasLennox/gradle-ci/badges/master/pipeline.svg)](https://gitlab.com/NicholasLennox/gradle-ci/-/commits/master)

<div id="top"></div>

<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://github.com/github_username/repo_name">
    <img src="images/IconDiablo.png" alt="Logo" width="80" height="80">
  </a>

<h3 align="center">RPG Characters</h3>

  <p align="center">
    Build a console application in Java
  </p>
</div>



<!-- TABLE OF CONTENTS -->

  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li><a href="#functionality">Functionality</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
  </ol>




<!-- ABOUT THE PROJECT -->
## About The Project

 <a href="https://github.com/github_username/repo_name">
    <img src="images/MainDiablo.png" alt="app">
  </a>

This is my first Java Assignment Project for the Noroff Java Bootcamp.
Where we were tasked with creating a minor RPG Characters app that consists of a Main Character class with subclasses such as mage, warrior, rogue and ranger. Where the characters should be able to equip weapons and armour and have scaling stats.

<p align="right">(<a href="#top">back to top</a>)</p>



### Built With

* [Java](https://www.java.com/nl/)
* [Gradle](https://gradle.org/)
* [Intellij](https://www.jetbrains.com/idea/)

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- GETTING STARTED -->
<!-- ## Getting Started

This project uses node and npm. Go check them out if you don't have them locally installed.
Project is also hoste on [Heroku](https://dry-citadel-05217.herokuapp.com)

```sh
$ npm init
# Install the dependencies for the project.
$ npm start
# Run a local React server
``` -->


<!-- USAGE EXAMPLES -->
## Functionality

<p>Character</p>
<ul>
<li>Can create characters of certain classes(mage, warrior etc)</li>
<li>Characters can equip weapons based on class</li>
<li>Characters can equip Armour based on class</li>
<li>Characters can level up</li>
<li>Characters can have scaling DPS based on class</li>
<li>Characters stats can be displayed</li>
</ul>
<p>Gear</p>
<ul>
<li>Can create Gear - weapons or armour</li>
<li>All gear stats are set during creation</li>

</ul>


<p align="right">(<a href="#top">back to top</a>)</p>



<!-- ROADMAP -->






<!-- CONTRIBUTING -->






<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE.txt` for more information.

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- CONTACT -->
## Contact

Axl Branco Duarte - axl.nl@hotmail.com

<p align="right">(<a href="#top">back to top</a>)</p>



<!-- ACKNOWLEDGMENTS -->





<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
